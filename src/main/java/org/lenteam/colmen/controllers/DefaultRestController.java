package org.lenteam.colmen.controllers;

import org.lenteam.colmen.entities.PersonEntity;
import org.lenteam.colmen.entities.SiteEntity;
import org.lenteam.colmen.services.DefaultService;
import org.lenteam.colmen.services.SitesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.AccessType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Hashtable;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Anton_Solovev
 * @since 8/20/2016
 *
 * this class will be deleted
 */
@RestController
@RequestMapping("")
public class DefaultRestController {
    DefaultService service;
    public DefaultRestController(DefaultService service) {
        this.service = service;
    }

    //@RequestMapping(path = "/{name}", method = POST)
    //public void addDefaultPerson(@RequestParam(defaultValue = "King Kong") String name) { service.saveWithName(name);}

}
