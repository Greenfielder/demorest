package org.lenteam.colmen.controllers;

import org.lenteam.colmen.entities.SiteEntity;
import org.lenteam.colmen.services.DefaultService;
import org.lenteam.colmen.services.SitesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Hashtable;

import static javax.swing.text.html.FormSubmitEvent.MethodType.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Mickey on 05.09.2016.
 */

@RestController
@RequestMapping("/sites")
public class SitesController {
// получает список сайтов Запрос: GET /sites
    @Autowired
    private SitesService sitesService;

    public SitesController (SitesService sitesService) {
        this.sitesService = sitesService;
    }

    @RequestMapping(path = "", method = GET, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Hashtable<Long, SiteEntity> getAll(){ return sitesService.getAll();}
    

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addSite(@RequestParam(defaultValue = "mail.ru") String name) { sitesService.saveWithName(name);}
}
