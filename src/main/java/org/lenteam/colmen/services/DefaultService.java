package org.lenteam.colmen.services;

import org.lenteam.colmen.entities.PersonEntity;
import org.lenteam.colmen.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Hashtable;

/**
 * @author Anton_Solovev
 * @since 8/21/2016
 *
 * this class will be deleted
 */

@Component
public class DefaultService {

    Hashtable<Long, PersonEntity> persons = new Hashtable<Long, PersonEntity>();
    public DefaultService() {

        PersonEntity p = new PersonEntity();
        p.setId(1L);
        p.setName("Putin");
        persons.put(1L, p);

        p = new PersonEntity();
        p.setId(2L);
        p.setName("Medvedev");
        persons.put(2L, p);

        p = new PersonEntity();
        p.setId(3L);
        p.setName("Navalnyi");
        persons.put(3L, p);
    }
    public PersonEntity getPerson (Long id){
        if (persons.containsKey(id))
            return persons.get(id);
        else
            return null;
    }
    public Hashtable<Long, PersonEntity> getAll(){ return persons; }

    @Autowired
    private PersonRepository repository;

    public PersonEntity findOne(Long id) {
        return repository.findOne(id);
    }

//    public void saveWithName(String name) {
//      repository.save(new PersonEntity(name));
    }

