package org.lenteam.colmen.services;

import org.lenteam.colmen.entities.PersonEntity;
import org.springframework.stereotype.Component;

import java.util.Hashtable;

/**
 * Created by Mickey on 05.09.2016.
 */

@Component
public class PersonService {
    Hashtable<Long, PersonEntity> persons = new Hashtable<Long, PersonEntity>();
    public PersonService() {

        PersonEntity p = new PersonEntity();
        p.setId(1L);
        p.setName("Putin");
        persons.put(1L, p);

        p = new PersonEntity();
        p.setId(2L);
        p.setName("Medvedev");
        persons.put(2L, p);

        p = new PersonEntity();
        p.setId(3L);
        p.setName("Navalnyi");
        persons.put(3L, p);
    }
    public PersonEntity getPerson (Long id){
        if (persons.containsKey(id))
            return persons.get(id);
        else
            return null;
    }
    public Hashtable<Long, PersonEntity> getAll(){ return persons; }

}
